# Hartree-Fock Tutorial

A Self-Documented Implementation of the Restricted-Hartree-Fock Self-Consistent Field method to calculate molecular properties.

This project aims to take page 146 of the book "Modern Quantum Chemistry" by Szabo and Ostlund, expand all the necessary steps, and implement them in hopefully readable python.

It is written in a way to complement courses based on MQC by enabling to take a look at the matrices "on the fly" and change parameters on a whim to see their effect. With some reordering of the steps, it may also serve as a practical project for learning python.

# Installation and Running

All relevant code is contained within the file "Hartree-Fock Sample.ipynb", which can be copied to any local direcory.

Running the interactive notebook requires Jupyter, which can be acquired with the anaconda distribution (from www.anaconda.com).

To run the notebook, change to the folder containing the ipynb-file and run
```jupyter notebook```

A browser window should open, guiding the next steps.

The code can also be viewed as a non-interactive html-file or a non-annotated .py script.
