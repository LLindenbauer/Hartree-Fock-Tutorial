#!/usr/bin/env python
# coding: utf-8

# In[1]:


from __future__ import division
from __future__ import print_function


# # Hartree-Fock Tutorial
# 
# ## About this Document
# 
# This Jupyter Notebook is a guide to programming the Hartree-Fock Method in simple Python using a simple model system (H2, STO-3G). It is strongly influenced by the book "Modern Quantum Chemistry" by Szabo & Ostlund (Dover Publications) and presents an implementation of their algorithm in Python.
# 
# Check the end of the document on how to run this as an interactive version.
# 
# 
# ## Further Reading:
# 
# https://vergil.chemistry.gatech.edu/notes/intro_extruc/intro_estruc.html
# 
# https://github.com/pm133/SCF_Szabo - a C implementation of the Fortran Code from S&O
# 
# http://sirius.chem.vt.edu/wiki/doku.php?id=crawdad:programming:project3 - Programming Tutorials
# 
# 
# ## Szabo & Ostlund
# 
# Page 146, the SCF Procedure as a to-do list, cited directly.
# 
# 1. Specify a Molecule: The set of nuclear coordinates $\{R_A\}$, the set of nuclear charges $\{Z_A\}$, and the number of electrons $N$
# 1. Specify a basis set $\{\phi_\mu\}$
# 1. Calculate all required molecular ingegrals:
#     * $S_{\mu \nu}$
#     * $H^{core}_{\mu \nu}$ (requires $T$ and $V$)
#     * $(\mu \nu|\lambda \sigma)$
# 1. Diagonalize the overlap matrix $S$ and obtain a transformation matrix $X$ from either (3.167) or (3.169)
# 1. Obtain a guess at the density matrix P
# 1. Calculate the matrix $G$ of equation (3.154) from the density matrix P and the two-electron integrals $(\mu \nu|\lambda \sigma)$
# 1. Add $G$ to the core-Hamiltonian to obtain the Fock matrix $F = H^{core} + G$
# 1. Calculate the transformed Fock matrix $F^\prime = X^\dagger F X$
# 1. Calculate $C=XC^\prime$
# 1. Diagonalize $F^\prime$ to obtain $C^\prime$ and $\epsilon$
# 1. Form a new density matrix $P$ from $C$ using equation (3.145)
# 1. Determine whether the procedure has converged: Determine whether the new density matrix of the last step is the same as the previous density matrix, within a certain margin. If the procedure has not converged, go back to the step of the calculation of $G$ but use the new density matrix.
# 1. If the procedure has converged, then use the resultant solution, represented by $C$, $P$, $F$, etc. to calculate the values of interest

# ### Specify the Molecule
# 
# #### Set of nuclear coordinates
# 
# We will use cartesian coordinates: x, y and z. We could now make a convention to store them as three numbers in that order, but it will be more readable to always call them by name, so we will use a dictionary. Also, we don't know beforehand how many atoms we will have, so we will use a list (for each atom) of dictionaries (for the coordiantes). So, first make an empty list and call it RA, just like in the formula:

# In[2]:


RA = []


# Now, as a simple molecule let's start with Dihydrogen as Szabo and Ostlund worked out all the Numbers already and we have many opportunities to pause and check if we made an error on our way. So let's just have all numbers in atomic units and the distance at 1.4 atomic units, with one hydrogen in the origin and the other along the x-axis.

# In[3]:


RA.append({"x":0.0 , "y":0,  "z":0})
RA.append({"x":1.4632, "y":0,  "z":0})


# RA now contains our atomic coordinates. When we output it, the list is in a fixed order, but the coordinate axes are not:

# In[4]:


print("Atom Coordinates")
print(RA)


# But since we have labeled them, it's not a big deal - to have them more readable, we will want to call them by name anyway.

# #### Nuclear Charges
# 
# Now let's do the charges. These will also be a list - let's call it ZA. Since the order of the elements of a list is fixed, we will make it a convention to use the same order as the atoms in the RA list. Try to make these kind of conventions "the least surprising as possible".

# In[5]:


ZA = [1, 1]


# #### Number of Electrons
# 
# Here we have a choice: We can simply enter the overall number of electrons or we can use the charge of the whole molecule and let the program calculate the charge. Since we most often want to deal wich neutral or singly charged molecules, the last one seems to be the less hassle for the user and the way to go. The nuclear charge Znuc is the sum of the elements of the ZA array $Z_{nuc} = \sum_{A} Z_A$. The number of electrons N needed to give a desired charge is then $N=Z_{nuc}-charge$.
# 
# We will use a for-loop to sum the elements of the ZA array:

# In[6]:


Znuc = 0
for A in range(len(ZA)):
    Znuc = Znuc + ZA[A]


# We set the initial value of Znuc to 0. Then len(ZA) gives the number of items stored in ZA, which currently should be 2. With this, range(x) then creates a list of numbers from 0 to x-1 - in this case: [0, 1]. The for loop takes each element of the list, assigns it to the variable A and executes the command in the loop for each element. ZA[A] gives the A'th element of ZA. We will add this to Znuc and update Znuc with the new value. Let's check Znuc and calculate N

# In[7]:


charge = 0
N = Znuc - charge
print("Number of Electrons")
print(N)


# ### Set up the Basis Set
# 
# Basis sets define the functions that will be fed into the Hartree Fock method to find the wave functions of the electrons. Typically, a given basis set will use a certain type of prototype function and supply parameters to fill into the prototype function for different atom types.
# 
# The most popular prototype function used is the gaussian orbital, which has the radial part $\phi^{gaussian}(r) = n(\alpha)e^{-\alpha r^2}$ with $\alpha$ as the parameter and $n(\alpha) = {(\frac{2\alpha}{\pi})}^{\frac{3}{4}}$ being a normalization factor.
# 
# The radial part of atomic orbitals more closely resembles Slater functions of the form $\phi^{Slater}= n(\zeta)e^{-\zeta r}$. However, gaussian functions tend to be easier to compute. To bridge the gap, basis sets typically use a sum of gaussians to closely fit a given Slater function. This "contracted" gaussian orbital (CGO) is a linear combination of primitive gaussian functions (sometimes just called "gaussian primitives"): $\phi^{CGO}(r) = \sum_{i} d_i \phi_i(r)$. The contraction factors $d_i$ are also given as part of the basis set.
# 
# The angular part of all s-orbitals is 1. In this tutorial we will ignore orbitals of higher angular momentum. The formulae for these can be found in the literature, e.g. in Petersson and Hellsing: https://doi.org/10.1088/0143-0807/31/1/004
# 
# #### The STO-3G Basis Set
# 
# The STO-3G - "Slater Type Orbitals - 3 Gaussians" - set was created (along with the other STO-nG sets) by Hehre, Stewart and Pople in 1969 and uses 3 gaussian orbitals to approximate one Slater orbital. 
# 
# The basis set works by defining one particular contraction of gaussian orbitals and then scale them to the needed atom type. For $\zeta = 1.0$, the contraction is as this:
# 
# | i |   $d_i$    | $\alpha_i$ |
# |---|------------|------------|
# | 1 | `0.444635` | `0.109818` |
# | 2 | `0.535328` | `0.535328` |
# | 3 | `0.154329` | `2.22766`  |

# In[8]:


STO_3G = [{"d": 0.444635, "a": 0.109818},
          {"d": 0.535328, "a": 0.405771},
          {"d": 0.154329, "a": 2.22766 }]


# #### Zeta Levels
# 
# Slater orbitals use the parameter $\zeta$ to scale the radial part, which is $(\zeta^3/\pi)^{1/2}e^{-\zeta r}$. The STO basis sets make use of this by defining a set of contracted gaussians and use the $\zeta$ to scale them to the correct size, depending on the atom type. The relationship between $\alpha$ and $\zeta$ is $\frac{\zeta^\prime}{\zeta}=(\frac{\alpha^\prime}{\alpha})^{\frac{1}{2}}$ or $\alpha(\zeta) = \alpha_{(\zeta = 1)}*\zeta^2$. So, what we want is to enter for each atom type a suitable value for $\zeta$ and have the program figure out the scaling and normalisation ( $n(\alpha) = {(\frac{2\alpha}{\pi})}^{\frac{3}{4}}$ ) for the primitive gaussians.

# In[9]:


def prim_gaussian_alpha(alpha_0, zeta):
    return alpha_0 * zeta * zeta

def prim_gaussian_norm(alpha_0, zeta):
    from math import pow, pi
    return pow((2*alpha_0*zeta*zeta/pi), 0.75)


# #### Saving the basis set
# 
# The hydrogen 1s orbitals does appear two times in our system: one centered on atom A, the other on atom B. For more complex systems, we will need to keep track of which orbital belongs to which atom. We will now create the data structure to save the basis set for a specific system:
# 
# - The basis set is a list of all orbitals
#     - The orbital has a center
#         - The center is identified by the number of the atom in the ZA - array
#     - The orbital has a contraction
#         - A contraction is a list of values for the gaussians that make up the contraction
#              - A gaussian is a dictionary containing the values for d, $\alpha$ and the normalisation constant

# In[10]:


from math import pi, pow

# H_1s_contraction = []
# H_1s_contraction.append({"d":0.444635, "alpha":0.168856, "norm": pow((2*0.168856/pi), 3/4)})
# H_1s_contraction.append({"d":0.535328, "alpha":0.623913, "norm": pow((2*0.623913/pi), 3/4)})
# H_1s_contraction.append({"d":0.154329, "alpha":3.42525 , "norm": pow((2*3.42525 /pi), 3/4)})
# basis_set = []
# basis_set.append({"center":0, "contraction": H_1s_contraction})
# basis_set.append({"center":1, "contraction": H_1s_contraction})
# basis_set.append({"center":2, "contraction": H_1s_contraction})
# basis_set.append({"center":3, "contraction": H_1s_contraction})

# or written in in a more functional way:

def get_primitive(STO_parameters, mu, zeta):
    return {"d":     STO_parameters[mu]["d"], 
            "alpha": prim_gaussian_alpha(STO_parameters[mu]["a"], zeta),
            "norm" : prim_gaussian_norm(STO_parameters[mu]["a"], zeta)}

def get_contraction(STO_parameters, zeta):
    return [get_primitive(STO_parameters, mu, zeta) for mu in range(len(STO_parameters))]

def get_zeta(Atom):
    zeta_list = {
        "H": 1.24,
        "He": 2.0925
    }
    if Atom in zeta_list.keys():
        return zeta_list[Atom]
    else:
        print("Unknown atom or zeta not available for: \"" + Atom + "\" - assuming zeta = 1")
        return 1

basis_set = []
basis_set.append({"center":0, "contraction": get_contraction(STO_3G, get_zeta("H"))})
basis_set.append({"center":1, "contraction": get_contraction(STO_3G, get_zeta("He"))})


# ### Calculate the Required Molecular Integrals
# 
# A computer is typically unable to efficiently or directly calculate the value of an integral given in the raw form of $\langle\phi|\psi\rangle$ or $\int{\phi(r)^*\psi(r) dr}$. There are computer algebra programs that can handle this type of input and there are methods for numerical integration ("quadrature"), but the most efficient way is to use analytical solutions whenever possible. For gaussian functions, such analytical solutions are readily available.
# 
# #### Overlap Integrals: $S_{\mu \nu}$
# 
# The overlap integrals are of the form $S_{\mu \nu} = \langle\phi_\mu|\phi_\nu\rangle$. The indices $\mu$ and $\nu$ represent the different basis functions. For contracted gaussians, they can be separated into overlaps between the contributing primitives to each CGO: $S_{\mu \nu} = \sum_{p} \sum_{q} d_{p\mu} d_{q\nu}S_{pq}$ with p and q being the index of the primitives of each CGO, the values d their respective contraction coefficients, and $S_{pq}$ the overlap between the primitives p and q.
# 
# The overlap integral between two primitive gaussians $\langle\phi_a|\phi_b\rangle$ has the analytical solution $S_{ab} = (\frac{\pi}{\alpha+\beta})^{\frac{3}{2}}e^{-\frac{\alpha\beta}{\alpha+\beta}{|R_a-R_b|^2}}$ with $\alpha$ and $\beta$ being the exponents of $\phi_a$ and $\phi_b$ respectively.
# 
# It will be convenient to spread out the computation of all these terms, so we will create a few functions to help organize the calculations:
# 
# - `distance3d`, when given two atom coordinates, returns the distance between them for use as $R_a-R_b$
# - `gaussian_overlap_integral`, when given the parameters of a two primitives p and q, returns their overlap $S_{pq}$
# - `contracted_gaussian_overlap_integral`, when given the indices of two basis functions, sums up all the needed $S_{pq}$ with the correct factors.
# 

# In[11]:


def distance3d(Point_A,Point_B):
    """
    Takes two coordinates as dictionaries in "x", "y", and "z" and returns their euclidian distance.
    """
    from math import sqrt, pow
    square_distance = 0;
    for dimension in "x", "y", "z":
        dimension_difference = Point_A[dimension] - Point_B[dimension]
        square_distance = square_distance + pow(dimension_difference, 2)
    return sqrt(square_distance)


# In[12]:


def gaussian_overlap_integral(phi_a,R_a,phi_b,R_b):
    """
    Takes two gaussian primitives and their centers and returns the overlap
    """
    from math import exp, pi, pow
    a = phi_a["alpha"]
    b = phi_b["alpha"]
    prefactor = pow((pi/(a+b)), 3/2) * phi_a["norm"] * phi_b["norm"]
    R = distance3d(R_a,R_b)
    return prefactor*exp(-a*b/(a+b)*pow(R, 2))


# In[13]:


def contracted_gaussian_overlap_integral(basis_set, RA, mu, nu):
    """
    Takes a basis set, a list of nuclear coordinates and the indices of two contracted gaussians
    returns their overlap
    """
    sum = 0
    phi_mu = basis_set[mu]
    phi_nu = basis_set[nu]
    R_mu = RA[phi_mu["center"]]
    R_nu = RA[phi_nu["center"]]
    contraction_mu = phi_mu["contraction"]
    contraction_nu = phi_nu["contraction"]
    for p in range(len(contraction_mu)):
        for q in range(len(contraction_nu)):
            d_mu_p = contraction_mu[p]["d"]
            d_nu_q = contraction_nu[q]["d"]
            sum_factor = d_mu_p * d_nu_q
            S_pq = gaussian_overlap_integral(contraction_mu[p], R_mu, contraction_nu[q], R_nu)
            sum = sum + (sum_factor * S_pq)
    return sum


# Now combine everything togehter into `S_mu_nu`, a two-dimensional array containing the $S_{\mu \nu}$-Matrix.
# 
# The overlap matrix is symmetric, so we would need to only compute one half-triangle of the matrix. However, we would have to make a convention on which half to use. Since this tutorial will focus on readybility, we will go the simple route and just compute them all.
# 
# Here we will use the the <a href="https://docs.python.org/3.5/tutorial/datastructures.html#list-comprehensions">List Comprehension</a> feature of python, which generates a list according to a function that operates on the members of another list. The outer list (`mu`) gets unpacked first, then for each `mu` the `nu` are unpacked and for every combination of these, the overlap integral function is called.

# In[14]:


S_mu_nu = [[contracted_gaussian_overlap_integral(basis_set, RA, mu, nu) 
           for nu in range(len(basis_set))]
          for mu in range(len(basis_set))]
for i in range(len(S_mu_nu)):
    print (S_mu_nu[i])


# #### Core Hamiltonian $H^{core}_{\mu \nu}$
# 
# The "Core Hamiltonian" is the part of the Hamilton operator that depends only on the intraction of one electron and the nuclei: The kinetic energy $T_{\mu \nu}$ in each of the orbitals of the basis set and the potential energy ${}^{Nuc}V_{\mu \nu}$ for each nucleus with all the orbitals. However, the complete classical meaning of these matrices is not readily explainable as they contain QM-coupling terms (see S&O pg. 161).
# 
# $H^{core}_{\mu \nu} = T_{\mu \nu} + \sum_{Nuc} ({}^{Nuc}V_{\mu \nu})$
# 
# ##### Kinetic Energy Integral
# 
# The definition and analytical form of the kinetic energy integral are:
# 
# $T_{\mu \nu} = (\phi_\mu|-\frac{1}{2}\nabla^2|\phi_\nu) = (\frac{\alpha \beta}{\alpha + \beta})(\frac{3-2\alpha\beta}{\alpha + \beta})(|R_a-R_b|^2)(\frac{\pi}{\alpha + \beta})^{(\frac{3}{2})}e^{-\frac{\alpha \beta}{\alpha + \beta}|R_a-R_b|^2}$
# 
# We will use the same strategy as before, by defining the appropriate functions for primitives and contractions and then use a list comprehension to bring the matrix togeher. The `distance3d` function will come in handy, too.

# In[15]:


def gaussian_kinetic_energy_integral_T(phi_a, R_a, phi_b, R_b):
    from math import exp, pi, pow
    a = phi_a["alpha"]
    b = phi_b["alpha"]
    norm_a = phi_a["norm"]
    norm_b = phi_b["norm"]
    R = distance3d(R_a,R_b)
    pre1 = a*b/(a+b)
    pre2 = (3-2*a*b/(a+b)*pow(R,2))
    pre3 = pow(pi/(a+b),(3/2))
    return (norm_a * norm_b * pre1 * pre2 * pre3 * exp(-(a*b)/(a+b)*pow(R,2)))
    
def contracted_kinetic_energy_integral_T(basis_set, RA, mu, nu):
    sum = 0;
    phi_mu = basis_set[mu]
    phi_nu = basis_set[nu]
    R_mu = RA[phi_mu["center"]]
    R_nu = RA[phi_nu["center"]]
    contraction_mu = phi_mu["contraction"]
    contraction_nu = phi_nu["contraction"]
    for p in range(len(contraction_mu)):
        for q in range(len(contraction_nu)):
            d_mu_p = contraction_mu[p]["d"]
            d_nu_q = contraction_nu[q]["d"]
            sum_factor = d_mu_p * d_nu_q
            factor = contraction_mu[p]["d"] * contraction_nu[q]["d"]
            T_pq = gaussian_kinetic_energy_integral_T(contraction_mu[p], R_mu, contraction_nu[q], R_nu)
            sum = sum + (factor * T_pq)
    return sum


# In[16]:


T_mu_nu = [[contracted_kinetic_energy_integral_T(basis_set, RA, mu, nu) 
           for nu in range(len(basis_set))]
          for mu in range(len(basis_set))]
print ("Kinetic Energy Matrix")
for i in range(len(T_mu_nu)):
    print (T_mu_nu[i])


# ##### Potential Energy Integral
# 
# The potential energy integrals ${}^{Nuc}V_{\mu \nu}$ need to be summed up over the number of cores. For each core $C$ it is given as ${}^{C}V_{\mu \nu} = (\phi_\mu|-\frac{Z_C}{r_{1C}}|\phi_\nu) = -\frac{2\pi}{\alpha + \beta}Z_Ce^{-\frac{\alpha \beta}{\alpha + \beta}|R_a-R_b|^2}F_0((\alpha + \beta)|R_P-R_C|^2)$ with
# 
# - $Z_C$ being the charge of nucleus $C$
# - $\alpha$ and $\beta$ being the exponents of the gaussian primitives $\phi_a$ and $\phi_b$ respectively
# - $F_0(t) = \frac{1}{2}(\frac{\pi}{t})^\frac{1}{2}{erf}(t^\frac{1}{2})$, where
#     - ${erf}(x)$ is the <a href="https://en.wikipedia.org/wiki/Error_function">Gauss error function</a>, which is available in the `scipy.special.erf` package
# - $R_P = \frac{\alpha R_a + \beta R_b}{\alpha + \beta}$ the center of the gaussian resulting from the reduction of two primitive gaussians
# 
# Now, those with a keen eye will spot a problem here: When we use this formula for two $\phi_a$ and $\phi_b$ where $a=b$ both are centered on core $C$, then one after the other, $r_{1C}$, $|R_P-R_C|$, and $t$ all become zero. This leads to two divisions by zero is $F_0(0) = \frac{1}{2}(\frac{\pi}{0})^\frac{1}{2}{erf}(0^\frac{1}{2})$. Luckily, we can lift the singularity by noting that $\lim_{t\to0} F_0(t) = 1-\frac{t}{3}$ (as implemented by S&O in Appendix B). This is also useful for larger molecules where $R_P$ might come close to a nucleus.
# 
# We might implement this as a check if the argument fo `F_zero` is equal to zero and then just return 1. However, as we are doing numeric computations with floating point numbers, it is advantageous to check if it is below a certain threshold and then apply the approximation.

# In[17]:


def gaussian_center(phi_a, R_a, phi_b, R_b):
    a = phi_a["alpha"]
    b = phi_b["alpha"]
    dividend = a + b
    x = (a*R_a["x"] + b*R_b["x"]) / dividend
    y = (a*R_a["y"] + b*R_b["y"]) / dividend
    z = (a*R_a["z"] + b*R_b["z"]) / dividend
    return {"x":x, "y":y, "z":z}

def F_zero(t):
    # first check if the argument is below a threshold
    if (t < 1e-6):
        return 1-t/3
    else:
        from scipy.special import erf
        from math import pi, sqrt
        return 1/2*sqrt(pi/t)*erf(sqrt(t))    

def gaussian_potential_energy_integral_V(phi_a, R_a, phi_b, R_b, Z_c, R_c):
    from math import exp, pi, pow
    a = phi_a["alpha"]
    b = phi_b["alpha"]
    norm_a = phi_a["norm"]
    norm_b = phi_b["norm"]
    R_ab = distance3d(R_a,R_b)
    pre1 = -2*pi/(a+b)*Z_c
    pre2 = exp(-a*b/(a+b)*R_ab*R_ab)
    R_p = gaussian_center(phi_a, R_a, phi_b, R_b)
    R_pc = distance3d(R_p, R_c)
    F_val = F_zero((a+b)*R_pc*R_pc)
    return (norm_a * norm_b * pre1 * pre2 * F_val)

def contracted_gaussian_potential_energy_integral_V(basis_set, RA, mu, nu, C):
    sum = 0
    phi_mu = basis_set[mu]
    phi_nu = basis_set[nu]
    R_mu = RA[phi_mu["center"]]
    R_nu = RA[phi_nu["center"]]
    R_c  = RA[C]
    Z_c  = ZA[C]
    contraction_mu = phi_mu["contraction"]
    contraction_nu = phi_nu["contraction"]
    for p in range(len(contraction_mu)):
        for q in range(len(contraction_nu)):            
            d_mu_p = contraction_mu[p]["d"]
            d_nu_q = contraction_nu[q]["d"]
            sum_factor = d_mu_p * d_nu_q
            V_pq = gaussian_potential_energy_integral_V(contraction_mu[p], R_mu, contraction_nu[q], R_nu, Z_c, R_c)
            sum = sum + (sum_factor * V_pq)
    return sum


# Since $V$ needs to be calculated for every nucleus, we have a list of lists of lists, with the nuclei being the outermost elements.

# In[18]:


V_mu_nu_c = [[[contracted_gaussian_potential_energy_integral_V(basis_set, RA, mu, nu, C) 
               for nu in range(len(basis_set))]
              for mu in range(len(basis_set))]
             for C in range(len(RA))]

for c in range(len(RA)):
    print ("Potential Energy Matrix for Atom Nr. ", c+1)
    for i in range(len(basis_set)):
        print (V_mu_nu_c[c][i])


# ##### Assembling the Core Hamiltonian
# 
# With the Potential and Kinetic Energy integrals done, we can assemble the core Hamiltonian. This is a good time to switch from python lists into a model more suitable to matrix algebra. We will use the `numpy` package to do all our linear algebra from now on. For `numpy` to work, we need convert our lists into numpy arrays, which is easily done by the `numpy.array` function. Note here that `V_C` is now a list of numpy arrays.

# In[19]:


import numpy as np
T = np.array(T_mu_nu)
V_C = [np.array(V_mu_nu) for V_mu_nu in V_mu_nu_c]
H_core = T
for V_mu_nu in V_C:
    H_core = H_core + V_mu_nu
print("Core Hamiltonian:")
print(H_core)


#  ##### The Two-Electron Integrals $(\mu \nu|\lambda \sigma)$
#  
#  These are the integrals that bring in the repulsion between the electrons. The Integals are symmetric, such that exchanging the indices on one of the two sides or switching the two sides does not change the integral. This table gives a quick overview of integrals that are identical:
#  
# |(|A|B|$|$|C|D|)|
# |-|-|-|--|-|-|-|
# |(|B|A|\||C|D|)| 
# |(|B|A|\||D|C|)|
# |(|A|B|\||D|C|)|
# |(|C|D|\||A|B|)|
# |(|C|D|\||B|A|)|
# |(|D|C|\||B|A|)|
# |(|D|C|\||A|B|)|
# 
# The analytic formula for the evaluation of these integals is:
# 
# $(AB|CD) = \frac{2\pi^{(5/2)}}{(a+b)(c+d)(a+b+c+d)^{1/2}}exp({-\frac{ab}{a+b}|R_a-R_b|^2 - \frac{cd}{c+d}|R_c-R_d|^2}) F_0[(a+b)(c+d)(a+b+c+d)|R_P-R_Q|^2]$
# 
# with
# - a, b, c, and d being the exponents of the primitive gaussians A, B, C, and D, respectively
# - $F_0(t) = \frac{1}{2}(\frac{\pi}{t})^\frac{1}{2}erf(t^\frac{1}{2})$
# - $erf(x)$ being the error function
# - $R_P = \frac{a R_a + b R_b}{a + b}$ and $R_Q = \frac{c R_c + d R_d}{c + d}$ the centers of the gaussian resulting from the reduction of two primitive gaussians each
# 
# 

# In[20]:


def four_center_integral(phi_a, R_a, phi_b, R_b, phi_c, R_c, phi_d, R_d):
    from math import pi, exp, sqrt, pow
    a = phi_a["alpha"]
    b = phi_b["alpha"]
    c = phi_c["alpha"]
    d = phi_d["alpha"]
    na = phi_a["norm"]
    nb = phi_b["norm"]
    nc = phi_c["norm"]
    nd = phi_d["norm"]
    R_ab = distance3d(R_a, R_b)
    R_cd = distance3d(R_c, R_d)
    R_p = gaussian_center(phi_a, R_a, phi_b, R_b)
    R_q = gaussian_center(phi_c, R_c, phi_d, R_d)
    R_pq = distance3d(R_p, R_q)
    normalization = na * nb * nc * nd
    prefactor = 2 * pow(pi, 5/2) / ((a + b) * (c + d) *sqrt(a + b + c + d))
    exponent = -(a * b / (a + b) * R_ab * R_ab) -(c * d / (c + d) * R_cd * R_cd)
    F_zero_arg = (a + b) * (c + d) / (a + b + c + d) * R_pq * R_pq
    return normalization * prefactor * exp(exponent) * F_zero(F_zero_arg)
    
def contracted_four_center_integral(basis_set, RA, mu, nu, la, si):
    sum = 0
    phi_mu = basis_set[mu]
    phi_nu = basis_set[nu]
    phi_la = basis_set[la]
    phi_si = basis_set[si]
    R_mu = RA[phi_mu["center"]]
    R_nu = RA[phi_nu["center"]]
    R_la = RA[phi_la["center"]]
    R_si = RA[phi_si["center"]]
    contraction_mu = phi_mu["contraction"]
    contraction_nu = phi_nu["contraction"]
    contraction_la = phi_la["contraction"]
    contraction_si = phi_si["contraction"]
    for p in range(len(contraction_mu)):
        for q in range(len(contraction_nu)):
            for r in range(len(contraction_la)):
                for s in range(len(contraction_si)):
                    prim_mu = contraction_mu[p]
                    prim_nu = contraction_nu[q]
                    prim_la = contraction_la[r]
                    prim_si = contraction_si[s]
                    d_mu_p = prim_mu["d"]
                    d_nu_q = prim_nu["d"]
                    d_la_r = prim_la["d"]
                    d_si_s = prim_si["d"]
                    normalization = d_mu_p * d_nu_q * d_la_r * d_si_s
                    fourcenter = four_center_integral(prim_mu, R_mu, prim_nu, R_nu, prim_la, R_la, prim_si, R_si)
                    sum = sum + (normalization * fourcenter)
    return sum


# In[21]:


Four_Center_mu_nu_la_si = [[[[contracted_four_center_integral(basis_set, RA, mu, nu, la, si)
                              for nu in range(len(basis_set))]
                             for mu in range(len(basis_set))]
                            for la in range(len(basis_set))]
                           for si in range(len(basis_set))]

print("Four-Center-Integrals")
for i in range(len(basis_set)):
    for j in range(len(basis_set)):
        for k in range(len(basis_set)):
            for l in range(len(basis_set)):
                print(i, j, k, l, ": ", Four_Center_mu_nu_la_si[i][j][k][l])


# ### Diagonalize S to get the Transformation Matrix X
# 
# Diagonalizing a matrix is another linear algebra task best delegated to existing, proven solutions, such as provided by numpy.
# 
# We will use the symmetric orthogonalisation for its simplicity, which uses $S^-\frac{1}{2}$ as $X$, which will be gained by first getting the eigenvalues $s$ of $S$ via $s = U^\dagger SU$, taking the inverse square root of the diagonal matrix and then backtransforming as such: $X=S^{-\frac{1}{2}} = U (U^\dagger S U)^{-\frac{1}{2}}U^\dagger = U s^{-\frac{1}{2}}U^\dagger$
# 
# The `numpy.linalg` library is able to do most of the work for us. More information can be found at the numpy function refrence: https://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.eigh.html#numpy.linalg.eigh
# 
# We will need these functions:
# 
# - `numpy.linalg.eigh` will decompose a symmetric/hermitian matrix into its eigenvalues and eigenvectors, both conveniently stored in appropriate arrays. The function will give two return values: the diagonal matrix of eigenvalues and the matrix of eigenvectors. We can assign them both to two separate variables when they are separated by a comma.
# - `numpy.linalg.inv(array)` inverts the matrix `array`
# - the `transpose` method of a matrix transposes it
# - the `dot(D)` method gives the dot product of the matrix itself with the matrix D, in that order: `M.dot(D)` equals $(M D)$
# 
# Most of these functions give back another `numpy` array, which python allows us to immediatly call other functions on, so $(A B) C$ becomes A.dot(B).dot(C). Be aware of the precence of the operations.
# 
# Unpacking a linear algebra formula in `numpy` is a bit unwieldy, so we introduce some intermediate steps. They are not truly necessary, but make the procedure much more readable.

# In[22]:


from numpy.linalg import eigh, inv

# Make an array out of the overlap matrix
S = np.array(S_mu_nu)

# Eigenvalue decomposition of S. Note that eigh(S) gives two return values, split apart by the comma
S_eigenvalues, U = eigh(S)

# Diagonalize S: s = (U_transpose.S).U
s = U.transpose().dot(S).dot(U)
s_sqrtinv = inv(np.sqrt(s))
X = U.dot(s_sqrtinv).dot(U.transpose())
print("Matrix X")
print(X)


# #### Alternative Way to generate X
# 
# A different, but equally usable X matrix can be generated by canonical orthogonalization (eq. 3.169-3.170): $X_{ij}=U_{ij}*s_j^{-1/2}$
# 
# For an intuitive way to grasp the different diagonalization methods, see S&O, page 174
# 
# It may be useful for numerical stability reasons, but here we can also use it to compare the results with the reference implementation. We also employ the sorting strategy by using `numpy.argsort` which gives us a permutation vector from the unsorted to the sorted array of the eigenvalues which we can then also apply to the eigenvectors.
# 
# Here we use a method called <a href="https://docs.python.org/3.7/tutorial/introduction.html">slicing</a>, where a list is operated on by a rule given in square brackets: `list[a:b:c]` gives a new list that boes from the item in `list` at position `a` to the item in position `b`, while taking a step of size `c`. Default values, when no values are given, are the beginning and end of the list, and a step of 1, respectively.

# In[23]:


def alternate_X(s,U):
    from math import sqrt
    import numpy as np
    # the [::-1] slice reverses the list. the step size of -1 reverses the order.
    sorting_permutation = np.argsort(s)[::-1]
    # it is not sorted form the largest to the smallest eigenvalue
    s = s[sorting_permutation]
    U = U[:, sorting_permutation]
    X = np.array([[U[i][j]/sqrt(s[j])
                  for j in range(len(s))]
                 for i in range(len(s))])
    return X
X = alternate_X(S_eigenvalues, U)
# also, for debugging reasons, switch the signs of the second column
X[0,1]= -X[0,1]
X[1,1]= -X[1,1]
print("Matrix X")
print(X)


# ### Obtain a Guess for the Density Matrix P
# 
# There are many ways to get a good guess for the density matrix. We will take the simplest one and set it to zero.

# In[24]:


from numpy import zeros
P = zeros([len(basis_set), len(basis_set)])

print("Initial Guess at the Density Matrix P")
print(P)


# ### Calculate the Matrix G
# 
# The matrix $G_{\mu \nu}$ "is the two-electron part of the Fock matrix". Equation 3.154 makes it to be
# $G_{\mu \nu} = \sum_{\lambda \sigma}P_{\lambda \sigma}((\mu \nu|\sigma \lambda) - \frac{1}{2}(\mu \lambda|\sigma \nu))$
# This is also the first part of the calculation that will get repeated until convergence of the self consistent field.

# In[25]:


def G_element(mu, nu, Pd):
    returnvalue = 0
    for la in range(len(basis_set)):
        for si in range(len(basis_set)):
            returnvalue = returnvalue + (
            Pd[la][si] * (
                Four_Center_mu_nu_la_si[mu][nu][si][la]
                - 0.5 * Four_Center_mu_nu_la_si[mu][la][si][nu]))
                
    return returnvalue

G = np.array([[G_element(mu, nu, P)
      for mu in range(len(basis_set))]
     for nu in range(len(basis_set))])

print ("Matrix \"G\"")
print (G)


# ### Obtain the Fock Matrix F
# 
# The Fock matrix $F_{\mu \nu}$ is the Sum of the $H^{core}$ and $G$ matrix.

# In[26]:


F = H_core + G

print ("The Fock Matrix")
print (F)


# ### Transform the Fock Matrix
# 
# Transform with the X matrix from before: $F^\prime = X^\dagger F X$

# In[27]:


F_prime = X.transpose().dot(F).dot(X)
print ("F-prime")
print (F_prime)


# ### Diagonalize $F^\prime$ to obtain $C^\prime$ and $\epsilon$
# 
# By equation 3.178, $F^\prime C^\prime = C^\prime \epsilon$ which is (another) eigenvalue problem. We already have `numpy.eigh` loaded, so this is simply:

# In[28]:


epsilon, C_prime = eigh(F_prime)
print ("C-prime")
print (C_prime)
print ("epsilon")
print (epsilon)

# for debugging and comparison, we use a different method
# suitable for 2x2 matrices from the reference implementation


def rot2x2(Fp):
    from math import pi, atan, sin, cos
    if abs(Fp[0,0] - Fp[1,1]) < 10e-10:
        theta = pi/4
    else:
        theta = 0.5*atan(2*Fp[0][1]/(Fp[0][0] - Fp[1][1]))
    print(Fp)
    print(theta)
    Cp = np.array(
         [[cos(theta),  sin(theta)],
          [sin(theta), -cos(theta)]])
    return Cp

# C_prime = rot2x2(F_prime)
print ("C-prime")
print (C_prime)


# ###  Calculate the Orbital Coefficients
# With $C=XC^\prime$

# In[29]:


C = X.dot(C_prime)
print("C")
print(C)


# ### Form a New Density Matrix $P$ from $C$
# Using equation (3.145): $P_{\mu \nu} = 2 \sum^{occupied}_{a}C_{\mu a}C^{*}_{\nu a}$
# Where a runs over all occupied orbitals. But we will save it as a new matrix - we will compare the P to check for convergence.

# In[30]:


def P_element(mu, nu, C):
    retval = 0
    for a in range(int(N/2)):
        retval = retval + 2*C[mu][a]*C[nu][a]
    return retval

P_new = np.array([[P_element(mu, nu, C)
         for mu in range(len(basis_set))]
        for nu in range(len(basis_set))])
print ("P new")
print (P_new)


# ### Has the procedure converged?
# 
# Determine whether the new density matrix of the last step is the same as the previous density matrix, within a certain margin. An appropriate measure is given on page 149: $(K^{-2}\sum_\mu\sum_\nu(P^{new}_{\mu\nu}-P^{old}_{\mu\nu})^2)^{1/2} = \delta$. If the value is smaller than $10^{-4}$, the Energy should be off by less then $10^{-6}$ Hartree.

# In[31]:


def delta_P_Pold(Pn, Po):
    from math import sqrt
    sum = 0
    for mu in range(len(basis_set)):
        for nu in range(len(basis_set)):
            sum = sum + (Pn[mu][nu] - Po[mu][nu]) * (Pn[mu][nu] - Po[mu][nu])
    sum = sum / (len(basis_set)*len(basis_set))
    sum = sqrt(sum)
    return sum

print ("Delta P")
print (delta_P_Pold(P_new, P))


# If the procedure has not converged, go back to the step of the calculation of $G$ but use the new density matrix.
# 
# We also want to avoid running into an infinite loop that never reaches a convergence, so we set a maximum of 10 iterations.
# 
# We will count the iterations from 1. Since we already did go around the equations once after setting up the initial guess, we enter the loop in iteration 2.
# 
# ### Form a new $G$ from the new $P$
# 
# We willl do this inside the loop. This is straightforward - we give the function `G_element(mu, nu, P)` the new $P$ as an argument. 

# In[32]:


current_iteration = 2
max_iterations = 10

while (delta_P_Pold(P_new,P) > 0.0001) and (current_iteration < max_iterations):
    
    P = P_new
    
    print ("This is iteration " + str(current_iteration))
    current_iteration = current_iteration + 1

    G = np.array([[G_element(mu, nu, P)
                   for mu in range(len(basis_set))]
                  for nu in range(len(basis_set))])

    print ("Matrix \"G\"")
    print (G)
    

    F = H_core + G

    print ("The Fock Matrix")
    print (F)

    F_prime = X.transpose().dot(F).dot(X)
    print ("F-prime")
    print (F_prime)

    epsilon, C_prime = eigh(F_prime)
    print ("C-prime")
    print (C_prime)
    print ("epsilon")
    print (epsilon)


    C = X.dot(C_prime)
    print("C")
    print(C)
    
    P_new = np.array([[P_element(mu, nu, C)
                       for mu in range(len(basis_set))]
                      for nu in range(len(basis_set))])
    print ("P new")
    print (P_new)
    print ("Delta P:\n" + str(delta_P_Pold(P_new,P)))
    
    print ("\n\n")
    
if (delta_P_Pold(P_new,P) < 0.0001):
    print("Convergence has been reached")
if (current_iteration > max_iterations):
    print("Maximum Number of Iterations reached")


# ## SCF Done
# Now, from P, C, F, the properties can be calculated.
# 
# ### Energies
# 
# The eigenvalues of $F^\prime$ are the orbital energies $\epsilon_i$. The values of $\epsilon$ for occupied orbitals are a reasonable prediction for the ionization potentials (but for the unoccupied ones, the values are not useful). The total Energy is a sum of the electronic and nuclear Energies.
# 
# #### Electronic Energy
# 
# The electronic energy can be calcuated from matrix elements during the SCF procedure by a formula given in (3.184): $E_0 = \frac{1}{2}\sum_{\mu \nu}P_{\nu \mu}(H_{\mu \nu}^{core} + F_{\mu \nu})$
# 
# #### Nuclear Energy
# 
# The nuclear energy contains the repulsion between the nuclei and since the atoms do not move during the SCF procedure, this term is constant as $E_{nuc} = \sum_A \sum_{B>A}\frac{Z_A Z_B}{R_{AB}}$ with A and B being the indices of the nuclei and $R_{AB}$ being the distance between two nuclei. We already have a function that gives us the distance, so we just have to loop over the nuclei.

# In[33]:


def electronic_energy(P, H, F, basis_set):
    energy = 0
    for mu in range(len(basis_set)):
        for nu in range(len(basis_set)):
            energy = energy + P[nu][mu] * (H[mu][nu] + F[mu][nu])
    energy = energy * 0.5
    return energy

def nuclear_energy(RA, ZA):
    energy = 0
    for A in range(len(RA)):
        for B in range(A+1,len(RA)):
            energy = energy + (ZA[A]*ZA[B]/distance3d(RA[A], RA[B]))
    return energy

print ("Nuclear Energy: ")
print (nuclear_energy(RA, ZA))
print ("Electronic Energy: ")
print (electronic_energy(P, H_core, F, basis_set))
print ("Total Energy")
print (nuclear_energy(RA, ZA) + electronic_energy(P, H_core, F, basis_set))


# ### Mulliken Populations
# 
# The matrix $PS$ contains information on the position of the electrons. The diagonal elements $(PS)_{\mu \mu}$ (S&O 3.195) give the amount of electrons in basis function $\mu$. If each basis functions can be said to be centered around   an atom, the sum of electrons in each basis function then gives the number of electrons at that atom.
# 
# This is not the only possible population analysis, there is also the Löwdin analysis that uses the diagonal elements $(S^{-0.5}PS^{0.5})_{\mu \mu}$

# In[34]:


print("The Mulliken Matrix")
print (P.dot(S))


# ## Running an Interactive Version of this Document
# 
# To use an interactive version of this document, download the files from https://gitlab.com/LLindenbauer/orbital-integration and run
# ```
# jupyter notebook
# ```
# from the folder where you have downloaded the files to.
# 
# Jupyter is readily available as part of the Anaconda Python distribution than can be downloaded from https://www.anaconda.com/
# 
# ## About the Author
# 
# This document was created by Leopold Lindenbauer in 2019 and is released under the MIT License (see the LICENSE file included in this project or https://opensource.org/licenses/MIT)
